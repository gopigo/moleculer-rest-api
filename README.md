# Moleculer-api-rest

[5 easy steps to create your REST microservice in NodeJS](https://medium.com/moleculer/5-easy-steps-to-create-your-rest-microservice-in-nodejs-94aede3249fc)

`$ moleculer init project-simple <PROJECT>` does not add Docker files
`$ moleculer init project <PROJECT>` adds Docker files


Not using NATS (option: Would you like to communicate with other nodes? NO)

## Build Setup

``` bash
# Install dependencies
npm install

# Start development mode with REPL
npm run dev

# Run unit tests
npm test

# Run continuous test mode
npm run ci

# Run ESLint
npm run lint
```