"use strict";

const DbService = require("moleculer-db");
const MongoAdapter = require("moleculer-db-adapter-mongo");

module.exports = {
	name: "products",
	mixins: [DbService],
	adapter: new MongoAdapter("mongodb://udx:Agosto2014@192.168.61.216:27017/moleculer"),
    collection: "products"
};